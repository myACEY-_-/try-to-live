﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : LivingEntity
{
    public enum State {idle, Chasing, Attacking };
    State curState;

    public float speed;
    NavMeshAgent nv;

    LivingEntity targetEntity;
    Transform target;
    bool hasTarget = true;

    //public float attackDistance = .0f;
    float timeBetweenAttack = 1;
    public float damage = 1;
    float nextAttackTime;

    float myCollisionRadius;
    float targetCollisionRadius;


    Material skinMaterial;
    Color originalColor;
    public ParticleSystem deathEffect;

	void Awake()
	{
		nv = GetComponent<NavMeshAgent>();

		if (GameObject.FindGameObjectWithTag("Player") != null)
		{
			hasTarget = true;

			target = GameObject.FindGameObjectWithTag("Player").transform;
			targetEntity = target.GetComponent<LivingEntity>();

			myCollisionRadius = GetComponent<CapsuleCollider>().radius;
			targetCollisionRadius = target.GetComponent<CapsuleCollider>().radius;
		}
	}

	protected override void Start()
	{
		base.Start();

		if (hasTarget)
		{
			targetEntity.OnDeath += OnTargetDeath;
			curState = State.Chasing;
			StartCoroutine(UpdatePath());
		}
	}

	public void SetCharacteristics(float moveSpeed, int hitsToKillPlayer, float enemyHealth, Color skinColour)
	{
		if (targetEntity != null)
		{
			nv.speed = moveSpeed;

			damage = targetEntity.maxHealth / hitsToKillPlayer;
			maxHealth = enemyHealth;

			skinMaterial = GetComponent<Renderer>().sharedMaterial;
			skinMaterial.color = skinColour;
			originalColor = skinMaterial.color;
		}
	}

	public override void TakeHit(float damage, Vector3 hitPoint, Vector3 hitDirection)
	{
		if (damage >= health)
		{
			Destroy(Instantiate(deathEffect.gameObject, hitPoint, Quaternion.FromToRotation(Vector3.forward, hitDirection)), deathEffect.startLifetime);
			Collider[] colliders = Physics.OverlapSphere(transform.position, 16);
			foreach (Collider nearbyObject in colliders)
			{
				//add force

				TurretController turret = nearbyObject.GetComponent<TurretController>();
				if (turret != null)
				{
					turret.EnemyKilled(gameObject.transform);
				}
			}
		}
		base.TakeHit(damage, hitPoint, hitDirection);
	}

	void Update()
	{

		if (hasTarget && curState == State.Chasing)
		{
			if (Time.time > nextAttackTime)
			{
				float sqrDstToTarget = (target.position - transform.position).sqrMagnitude;
				if (sqrDstToTarget < Mathf.Pow(attackDistance + myCollisionRadius + targetCollisionRadius, 2))
				{
					nextAttackTime = Time.time + timeBetweenAttack;
					StartCoroutine(Attack());
				}

			}
		}

	}

	IEnumerator Attack()
	{
		if (curState != State.idle)
		{
			curState = State.Attacking;
			nv.enabled = false;

			Vector3 originalPosition = transform.position;
			Vector3 dirToTarget = (target.position - transform.position).normalized;
			Vector3 attackPosition = target.position - dirToTarget * (myCollisionRadius);

			float attackSpeed = 3;
			float percent = 0;

			skinMaterial.color = Color.red;
			bool hasAppliedDamage = false;

			while (percent <= 1)
			{

				if (percent >= .5f && !hasAppliedDamage)
				{
					hasAppliedDamage = true;
					targetEntity.TakeDamage(damage);
				}

				percent += Time.deltaTime * attackSpeed;
				float interpolation = (-Mathf.Pow(percent, 2) + percent) * 4;
				transform.position = Vector3.Lerp(originalPosition, attackPosition, interpolation);

				yield return null;
			}

			skinMaterial.color = originalColor;
			curState = State.Chasing;
			nv.enabled = true;
		}
	}

	IEnumerator UpdatePath()
	{
		float refreshRate = .25f;

		while (hasTarget)
		{
			if (curState == State.Chasing)
			{
				Vector3 dirToTarget = (target.position - transform.position).normalized;
				Vector3 targetPosition = target.position - dirToTarget * (myCollisionRadius + targetCollisionRadius + attackDistance / 2);
				if (!dead)
				{
					nv.SetDestination(targetPosition);
				}
			}
			yield return new WaitForSeconds(refreshRate);
		}
	}

	void OnTargetDeath()
	{
		hasTarget = false;
		target = null;
		targetEntity = null;
		curState = State.idle;
	}
}
