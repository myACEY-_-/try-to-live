﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RandomSpawner : MonoBehaviour
{
	public bool devMode;

	public Enemy enemy;

	List<GameObject> enemiesAlive = new List<GameObject>();

	LivingEntity playerEntity;
	Transform playerT;

	Wave currentWave;
	int currentWaveNumber;

	[HideInInspector] public int enemiesRemainingToSpawn;
	[HideInInspector] public int enemiesRemainingAlive;
	float nextSpawnTime;

	RandomMapGenerator map;

	float timeBetweenCampingChecks = 2;
	float campThresholdDistance = 1.5f;
	float nextCampCheckTime;
	Vector3 campPositionOld;
	bool isCamping;

	bool isDisabled;
	
	public event System.Action<int> OnNewWave;
	public Wave[] waves;

	

	void Start()
	{
		playerEntity = FindObjectOfType<RandomPlayer>();
		playerT = playerEntity.transform;

		nextCampCheckTime = timeBetweenCampingChecks + Time.time;
		campPositionOld = playerT.position;
		playerEntity.OnDeath += OnPlayerDeath;

		map = FindObjectOfType<RandomMapGenerator>();
		NextWave();
	}

	void Update()
	{
		if (!isDisabled)
		{
			if (Time.time > nextCampCheckTime)
			{
				nextCampCheckTime = Time.time + timeBetweenCampingChecks;

				isCamping = (Vector3.Distance(playerT.position, campPositionOld) < campThresholdDistance);
				campPositionOld = playerT.position;
			}

			if ((enemiesRemainingToSpawn > 0 /*|| currentWave.infinite*/) && Time.time > nextSpawnTime)
			{
				enemiesRemainingToSpawn--;
				nextSpawnTime = Time.time + currentWave.timeBetweenSpawns;

				StartCoroutine("SpawnEnemy");
			}
		}

		if (devMode)
		{
			if (Input.GetKeyDown(KeyCode.Backspace))
			{
				StopCoroutine("SpawnEnemy");
				foreach (Enemy enemy in FindObjectsOfType<Enemy>())
				{
					GameObject.Destroy(enemy.gameObject);
				}
				NextWave();
			}
		}
	}

	IEnumerator SpawnEnemy()
	{
		float spawnDelay = 1;
		float tileFlashSpeed = 4;

		Transform spawnTile = map.GetRandomOpenTile();
		if (isCamping)
		{
			spawnTile = map.GetTileFromPosition(playerT.position);
		}
		Material tileMat = spawnTile.GetComponent<Renderer>().material;
		Color initialColour = Color.white;
		Color flashColour = Color.red;
		float spawnTimer = 0;

		while (spawnTimer < spawnDelay)
		{

			tileMat.color = Color.Lerp(initialColour, flashColour, Mathf.PingPong(spawnTimer * tileFlashSpeed, 1));

			spawnTimer += Time.deltaTime;
			yield return null;
		}

		Enemy spawnedEnemy = Instantiate(enemy, spawnTile.position + Vector3.up, Quaternion.identity);
		spawnedEnemy.OnDeath += OnEnemyDeath;
		spawnedEnemy.SetCharacteristics(currentWave.moveSpeed, currentWave.hitsToKillPlayer, currentWave.enemyHealth, currentWave.skinColour);
		enemiesAlive.Add(spawnedEnemy.gameObject);
	}

	void OnPlayerDeath()
	{
		isDisabled = true;
	}

	void OnEnemyDeath()
	{
		enemiesRemainingAlive--;
		if (enemiesRemainingAlive == 0)
		{
			NextWave();
		}
	}
	void ResetPlayerPosition()
	{
		playerT.position = map.GetTileFromPosition(Vector3.zero).position + Vector3.up;
	}

	void NextWave()
	{
		foreach (Transform child in gameObject.transform)
		{
			Destroy(child.gameObject);
		}
		currentWaveNumber++;
		if (currentWaveNumber - 1 < waves.Length)
		{
			currentWave = waves[currentWaveNumber - 1];
			RandomizeWaveCounts();

			enemiesRemainingToSpawn = currentWave.enemyCount;
			enemiesRemainingAlive = enemiesRemainingToSpawn;

			if (OnNewWave != null)
			{
				OnNewWave(currentWaveNumber);
			}
			ResetPlayerPosition();
		}
	}

	[System.Serializable]
	public class Wave
	{
		public bool infinite;
		public int enemyCount;
		public float timeBetweenSpawns;

		public float moveSpeed;
		public int hitsToKillPlayer;
		public float enemyHealth;
		public Color skinColour;
	}

	void RandomizeWaveCounts()
	{
		//currentWave.infinite = false;

		//enemyCount
		if (10 * currentWaveNumber < 130)
			currentWave.enemyCount = currentWaveNumber * 10;
		else
			currentWave.enemyCount = Random.Range(100, 140);
		
		//currentWave.enemyCount =2;
		if (currentWaveNumber > 9)
			currentWave.timeBetweenSpawns = (1 / ((float)currentWaveNumber / 10)) * 0.7f;
		else
			currentWave.timeBetweenSpawns = 0.7f;
		//print(currentWave.timeBetweenSpawns);

		currentWave.moveSpeed = Random.Range(7, 9);
		//currentWave.moveSpeed = 0;
		currentWave.hitsToKillPlayer = Random.Range(3, 4);
		if (currentWaveNumber > 3)
			currentWave.enemyHealth = 2;
		else
			currentWave.enemyHealth = 1;

			currentWave.skinColour = new Color(Random.value, Random.value, Random.value, 1.0f);
	}

}