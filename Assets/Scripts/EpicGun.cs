﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EpicGun : MonoBehaviour
{
    public GameObject gunPrefab;
    public GameObject rotatingGun;
    Animator anim;
    bool CanPickUp = false;
    GameObject player;
    [HideInInspector] public GameObject pressEToUse;
    void Start()
    {
        anim = GetComponent<Animator>();
        pressEToUse = FindObjectOfType<Canvas>().GetComponent<RandomGameUI>().pressEToUse;
    }
    
    void Update()
    {
        rotatingGun.transform.Rotate(0, 0.1f, 0.0f, Space.World);
        if(CanPickUp == true && Input.GetKeyDown(KeyCode.E))
            givePlayerGun(player.GetComponent<RandomGunController>());
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.GetComponent<RandomGunController>() != null)
        {
            CanPickUp = true;
            player = other.gameObject;
            anim.SetBool("OnTriggerEnter", true);
            pressEToUse.SetActive(true);
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<RandomGunController>() != null)
        {
            anim.SetBool("OnTriggerEnter", false);
            CanPickUp = false;
            player = null;
            pressEToUse.SetActive(false);
        }
    }


    void givePlayerGun(RandomGunController pl)
    {
        pl.RecieveEpicWeapon(gunPrefab);
        anim.SetBool("OnTriggerEnter", false);
        anim.SetBool("destroy", true);
    }

    public void Destroy()
    {
        Destroy(gameObject);
    }
}
