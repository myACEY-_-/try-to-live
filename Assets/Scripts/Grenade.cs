﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Grenade : MonoBehaviour
{
    public float timeToDestroy;
    public int damage;
    public float radius = 5;
    public float startRadius;

    public GameObject boomEffect;

    public Image imageColor;
    public Transform Image;

    RectTransform rt;
    void Start()
    {
        imageColor.color = new Color32(0, 0, 100, 0);
        rt = Image.GetComponent<RectTransform>();
        GetComponent<Rigidbody>().AddRelativeForce(Vector3.forward * 200);
        StartCoroutine("TimeToExplode");
        StartCoroutine("FadeImage");
    }
    private void Update()
    {
        Image.rotation = Quaternion.Euler(-90, Image.rotation.y, Image.rotation.z);
    }
    IEnumerator TimeToExplode()
    {
        yield return new WaitForSeconds(timeToDestroy);
        Boom();
    }
    void Boom()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, radius);
        foreach (Collider nearbyObject in colliders)
        {
            Enemy enemy = nearbyObject.GetComponent<Enemy>();
            if(enemy != null)
            {
                Vector3 direction = enemy.transform.position + transform.position;
                RaycastHit hit;
                Physics.Raycast(transform.position, enemy.transform.position, out hit);
                Debug.DrawRay(transform.position, enemy.transform.position, Color.red);
                enemy.TakeHit(damage, hit.point, -direction);
            }
        }
        GameObject effect = Instantiate(boomEffect, transform.position, Quaternion.identity);
        Destroy(effect, boomEffect.GetComponent<ParticleSystem>().main.duration);
        Destroy(gameObject);
    }

    IEnumerator FadeImage()
    {
        float percent = 0;
        float speed = 1/timeToDestroy;
        Color32 zeroColor = new Color32(252, 252, 252, 0);
        Color32 color = new Color32(252, 252, 252, 160);
        while (percent < 1)
        {
            percent += Time.deltaTime * speed;
            imageColor.color = Color32.Lerp(zeroColor, color, percent);
            rt.sizeDelta = Vector2.Lerp(new Vector2(startRadius,startRadius), new Vector2(radius * 2, radius * 2), percent);
            yield return null;
        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
     //Use the same vars you use to draw your Overlap SPhere to draw your Wire Sphere.
     Gizmos.DrawWireSphere(transform.position, radius);
    }
}
