﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : LivingEntity
{
    float speed = 10;
    public float destroyTime = 6;
    public LayerMask collisionMask;
    public LayerMask obstacleCollisionMask;

    [HideInInspector]public float damage;

    float skinWidth = .1f;

    private void Start()
    {
        Destroy(gameObject, destroyTime);
        Collider[] initialCollisions = Physics.OverlapSphere(transform.position, .1f, collisionMask);
        if(initialCollisions.Length > 0)
        {
            onHitObject(initialCollisions[0], transform.position);
        }
    }

    public void SetParametrs(float newSpeed, float dmg)
    {
        speed = newSpeed;
        damage = dmg;
    }

    void Update()
    {
        float moveDist = speed * Time.deltaTime;
        CheckCollisions(moveDist);
        transform.Translate(Vector3.forward * moveDist);
    }

    void CheckCollisions(float moveDistance)
    {
        Ray ray = new Ray(transform.position, transform.forward);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, moveDistance + skinWidth, collisionMask, QueryTriggerInteraction.Collide))
            onHitObject(hit.collider, hit.point);

        if (Physics.Raycast(ray, out hit, moveDistance + skinWidth, obstacleCollisionMask, QueryTriggerInteraction.Collide))
            Destroy(gameObject);
    }
    

    void onHitObject(Collider c, Vector3 hitPoint)
    {
        IDamagable damagableObject = c.GetComponent<IDamagable>();
        if (damagableObject != null)
            damagableObject.TakeHit(damage, hitPoint, transform.forward);
        Destroy(gameObject);
    }
}
