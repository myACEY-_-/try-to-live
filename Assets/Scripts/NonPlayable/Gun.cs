﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
	public enum GunType { Common, GrenadeLauncher}
	public GunType gunType;
	public enum FireMode { Auto, Burst, Single };
	public FireMode fireMode;

	public List<Transform> shootPoints = new List<Transform>();

	public float shootPointRot;
	public GameObject projectile;
	public float msBetweenShots = 100;
	public float muzzleVelocity = 35;
	public float damage;
	public int burstCount;
	public int projectilesPerMag;
	public float reloadTime = .3f;

	[Header("Recoil")]
	public Vector2 kickMinMax = new Vector2(.05f, .2f);
	public Vector2 recoilAngleMinMax = new Vector2(3, 5);
	public float recoilMoveSettleTime = .1f;
	public float recoilRotationSettleTime = .1f;

	[Header("Effects")]
	public Transform shell;
	public Transform shellEjection;
	muzzleFlash muzzleflash;
	float nextShotTime;

	bool triggerReleasedSinceLastShot;
	int shotsRemainingInBurst;
	int projectilesRemainingInMag;
	bool isReloading;

	Vector3 recoilSmoothDampVelocity;
	float recoilRotSmoothDampVelocity;
	float recoilAngle;

	[HideInInspector] public AudioManager au;
	public string shootSoundName;
	public string reloadSoundName;

	[Header("Epic")]
	public GameObject grenade;


	void Start()
	{
		//au = FindObjectOfType<AudioManager>();
		muzzleflash = GetComponent<muzzleFlash>();
		shotsRemainingInBurst = burstCount;
		projectilesRemainingInMag = projectilesPerMag;
	}

	void Update()
	{
		// animate recoil
		transform.localPosition = Vector3.SmoothDamp(transform.localPosition, Vector3.zero, ref recoilSmoothDampVelocity, recoilMoveSettleTime);
		recoilAngle = Mathf.SmoothDamp(recoilAngle, 0, ref recoilRotSmoothDampVelocity, recoilRotationSettleTime);
		transform.localEulerAngles = transform.localEulerAngles + Vector3.left * recoilAngle;

		if (!isReloading && projectilesRemainingInMag == 0)
		{
			Reload();
		}
	}

	void Shoot()
	{

		if (!isReloading && Time.time > nextShotTime && projectilesRemainingInMag > 0)
		{
			if (fireMode == FireMode.Burst)
			{
				if (shotsRemainingInBurst == 0)
				{
					return;
				}
				shotsRemainingInBurst--;
			}
			else if (fireMode == FireMode.Single)
			{
				if (!triggerReleasedSinceLastShot)
				{
					return;
				}
			}
			if (gunType == GunType.Common)
			{
				for (int i = 0; i < shootPoints.Count; i++)
				{
					if (projectilesRemainingInMag == 0)
					{
						break;
					}
					projectilesRemainingInMag--;
					nextShotTime = Time.time + msBetweenShots / 1000;
					GameObject newProjectile = Instantiate(projectile, shootPoints[i].position, shootPoints[i].rotation);
					newProjectile.gameObject.GetComponent<Bullet>().SetParametrs(muzzleVelocity, damage);
				}

				Instantiate(shell, shellEjection.position, shellEjection.rotation);
				muzzleflash.Activate();
				transform.localPosition -= Vector3.forward * Random.Range(kickMinMax.x, kickMinMax.y);
				recoilAngle += Random.Range(recoilAngleMinMax.x, recoilAngleMinMax.y);
				recoilAngle = Mathf.Clamp(recoilAngle, 0, 30);
				au.Play(shootSoundName);
			}
			else if(gunType == GunType.GrenadeLauncher)
			{
				projectilesRemainingInMag--;
				nextShotTime = Time.time + msBetweenShots / 1000;
				GameObject newProjectile = Instantiate(grenade.gameObject, shootPoints[0].position, shootPoints[0].rotation);
				au.Play(shootSoundName);
			}

		}
	}

	public void Reload()
	{
		if (!isReloading && projectilesRemainingInMag != projectilesPerMag)
		{
			StartCoroutine(AnimateReload());
		}
	}

	IEnumerator AnimateReload()
	{
		isReloading = true;
		au.Play(reloadSoundName);
		yield return new WaitForSeconds(.2f);

		float reloadSpeed = 1f / reloadTime;
		float percent = 0;
		Vector3 initialRot = transform.localEulerAngles;
		float maxReloadAngle = 30;

		while (percent < 1)
		{
			percent += Time.deltaTime * reloadSpeed;
			float interpolation = (-Mathf.Pow(percent, 2) + percent) * 4;
			float reloadAngle = Mathf.Lerp(0, maxReloadAngle, interpolation);
			transform.localEulerAngles = initialRot + Vector3.left * reloadAngle;

			yield return null;
		}


		isReloading = false;
		projectilesRemainingInMag = projectilesPerMag;
	}

	public void Aim(Vector3 aimPoint)
	{
		if (!isReloading)
		{
			transform.LookAt(aimPoint);
		}
	}

	public void OnTriggerHold()
	{
		Shoot();
		triggerReleasedSinceLastShot = false;
	}

	public void OnTriggerRelease()
	{
		triggerReleasedSinceLastShot = true;
		shotsRemainingInBurst = burstCount;
	}
}
