﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LivingEntity : MonoBehaviour, IDamagable
{
    public float maxHealth;
    protected float health;
    public bool dead;

    public float attackDistance;

    public event System.Action OnDeath;

    protected virtual void Start()
    {
        health = maxHealth;
    }
    public virtual void TakeHit(float damage, Vector3 hitPoint, Vector3 hitDirection)
    {
        TakeDamage(damage);
    }

    public void TakeDamage(float damages)
    {
        health -= damages;
        if (health <= 0 && !dead)
            Die();
    }

    [ContextMenu("SelfDistract")]
    protected void Die()
    {
        dead = true;
        OnDeath?.Invoke();
        GameObject.Destroy(gameObject);
    }

}
