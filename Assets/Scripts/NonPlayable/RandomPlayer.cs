﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomPlayer : LivingEntity
{

    public float moveSpeed;
    public Crosshairs crosshairs;

    Camera cam;

    PlayerController controller;

    RandomGunController gun;

    [HideInInspector] public bool pause;

    public ParticleSystem teleportEffect;
    public ParticleSystem teleportEffectLight;
    protected override void Start()
    {
        base.Start();
    }

    void OnNewWave(int waveNum)
    {
        health = maxHealth;
        gun.EquipGun(waveNum); 
    }

    private void Awake()
    {
        Time.timeScale = 1;
        controller = GetComponent<PlayerController>();
        cam = Camera.main;
        gun = GetComponent<RandomGunController>();
        FindObjectOfType<RandomSpawner>().OnNewWave += OnNewWave;

    }
    void Update()
    {
        if (pause)
            return;
        #region moveInput
        Vector3 moveInput = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));
        Vector3 moveVelocity = moveInput.normalized * moveSpeed;
        controller.Move(moveVelocity);
        #endregion

        #region lookToMouse
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        Plane ground = new Plane(Vector3.up, Vector3.up * gun.GunHeight());

        float rayDist;
        if(ground.Raycast(ray, out rayDist))
        {
            Vector3 point = ray.GetPoint(rayDist);
            controller.LookAt(point);
            crosshairs.transform.position = point;
            crosshairs.DetectTarget(ray);
            if((new Vector2(point.x, point.z) - new Vector2(transform.position.x, transform.position.z)).sqrMagnitude > 4){
                gun.Aim(point);
            }
        }
        #endregion

        #region weaponInput
        if (Input.GetMouseButton(0))
            gun.OnTriggerHold();

        if (Input.GetMouseButtonUp(0))
            gun.OnTriggerRelease();

        if (Input.GetKeyDown(KeyCode.R))
        {
            gun.Reload();
        }

        if (Input.GetKeyDown(KeyCode.E))
            gun.SetTurret();
        #endregion
    }

    public void TeleportEffect()
    {
        teleportEffect.Play();
        teleportEffectLight.Play();
    }
}
