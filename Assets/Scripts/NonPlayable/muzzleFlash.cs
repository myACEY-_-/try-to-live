﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class muzzleFlash : MonoBehaviour
{
    public GameObject muzzleFlashHolder;
    public Sprite[] flashSprites;
    public SpriteRenderer[] spriteRenderers;

    public float flashTime;

    private void Start()
    {
        Diactivate();
    }
    public void Activate()
    {
        muzzleFlashHolder.gameObject.SetActive(true);

        int index = Random.Range(0, flashSprites.Length);
        for(int i = 0; i < spriteRenderers.Length; i++)
        {
            spriteRenderers[i].sprite = flashSprites[index];
        }
        Invoke("Diactivate", flashTime);
    }
    public void Diactivate()
    {
        muzzleFlashHolder.gameObject.SetActive(false);
    }
}
