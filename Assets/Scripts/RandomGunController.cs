﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomGunController : MonoBehaviour
{
    public Transform weaponHoldPoint;
    //public Gun[] allGuns;
    public Gun[] allModeles;
    Gun equippedGun;
    public GameObject shootPoint;

    int shootPointCount;

    int rand = 0;

    AudioManager au;

    RandomMapGenerator rmg;

    public List<GameObject> EpicGuns = new List<GameObject>();
    public GameObject turretSetter;

    GameObject generatedEpicWeapon;
    bool canSetTurret = false;
    turretSetter ts;
    GameObject tur;
    GameObject turret;

    int waveN= 1;
    private void Start()
    {

        au = FindObjectOfType<AudioManager>();

        rmg = FindObjectOfType<RandomMapGenerator>();
        rmg.rgc = GetComponent<RandomGunController>();
        au.Play("music" + 1);
    }
    public void EquipGun(Gun gunToEquip, int waveNum)
    {
        waveN = waveNum;
        if (turret != null)
            Destroy(turret.gameObject);
        turret = null;
        GenerateWeaponsOnMap();
        if (equippedGun != null)
        {
            Destroy(equippedGun.gameObject);
        }
        equippedGun = Instantiate(gunToEquip, weaponHoldPoint.position, weaponHoldPoint.rotation,weaponHoldPoint);
        randomizeWepon(equippedGun);
    }

    public void EquipGun(int waveNum)
    {
        int randomModel = Random.Range(0, allModeles.Length);
        EquipGun(allModeles[randomModel], waveNum);
        if (turret != null)
            Destroy(turret);
    }

    void EquipEpicWeapon(GameObject weapon)
    {
        if (equippedGun != null)
        {
            Destroy(equippedGun.gameObject);
        }
        equippedGun = null;
        GameObject weap = Instantiate(weapon.gameObject, weaponHoldPoint.position, weaponHoldPoint.rotation, weaponHoldPoint);
        equippedGun = weap.GetComponent<Gun>();
        equippedGun.au = au;
    }

    public void OnTriggerHold()
    {
        if (equippedGun != null)
            equippedGun.OnTriggerHold();
    }

    public void OnTriggerRelease()
    {
        if (equippedGun != null)
            equippedGun.OnTriggerRelease();
    }

    public float GunHeight()
    {
        return weaponHoldPoint.position.y;
    }
    public void Aim(Vector3 aimPoint)
    {
        if (equippedGun != null)
            equippedGun.Aim(aimPoint);

    }

    public void Reload()
    {
        if (equippedGun != null)
            equippedGun.Reload();

    }

    void randomizeWepon(Gun gun)
    {
        gun.au = au;
        gun.reloadSoundName = "reload" + 1;//Random.Range(1, 7);
        gun.shootSoundName = "shoot" + 1;//Random.Range(1, 5);
        //int randomPercent = Random.Range(0, 9);
        //if (randomPercent < waveN)
            //CreatingRandomShootPoints(gun, waveN);

       
        gun.msBetweenShots = Random.Range(100, 450);
        gun.muzzleVelocity = Random.Range(24, 37);

        gun.projectilesPerMag = Random.Range(8, 40);
        gun.reloadTime = Random.Range(0.2f, 0.5f);

        //Mode
        if (gun.shootPoints.Count == 1)
        {
            float randomNum = Random.Range(0, 2);
            if (randomNum == 0)
                gun.fireMode = Gun.FireMode.Auto;
            else if (randomNum == 1)
            {
                gun.fireMode = Gun.FireMode.Burst;
                gun.burstCount = Random.Range(3, 6);
                int r = Random.Range(5, 10);
                gun.projectilesPerMag = gun.burstCount * r;
            }
            else if (randomNum == 2)
                gun.fireMode = Gun.FireMode.Single;
        }
        else
        {
            int c = gun.shootPoints.Count;
            gun.projectilesPerMag = c * Random.Range(5, 8);
        }

    }

    void CreatingRandomShootPoints(Gun gun, int num)
    {
        transform.rotation = Quaternion.Euler(transform.rotation.x, 0, transform.rotation.z);

        shootPointCount = GetRandomInt();
        if (shootPointCount > 1)
        {
            /*if(gun.shootPoints.Count > 1) {
                print("+1");
                for (int i = 1; i < gun.shootPoints.Count; i++)
                {
                    gun.shootPoints.Remove(gun.shootPoints[i]);
                    print("+");
                }            
            }*/
            gun.shootPointRot = Random.Range(1, 15);
            float minRot = 0 - gun.shootPointRot;
            float nowRot = minRot;
            Quaternion r = Quaternion.Euler(gun.shootPoints[0].rotation.x, minRot, gun.shootPoints[0].rotation.z);
            gun.shootPoints[0].transform.localRotation = r;
            for (int i = 1; i < shootPointCount; i++)
            {
                nowRot += gun.shootPointRot / (shootPointCount / 2);
                Quaternion l = Quaternion.Euler(gun.shootPoints[0].localRotation.x, nowRot, gun.shootPoints[0].localRotation.z);
                Quaternion point = l;
                GameObject shootPointPrefab = Instantiate(shootPoint, gun.shootPoints[0].position, point, gun.shootPoints[0]);
                gun.shootPoints.Add(shootPointPrefab.transform);
            }
        }
    }

    int GetRandomInt()
    {
        if(waveN < 22)
            rand = Random.Range(2, waveN);
        else
            rand = Random.Range(2, 22);
        if (rand % 2 == 0) GetRandomInt();
        return rand;
    }

    void GenerateWeaponsOnMap()
    {
        if (generatedEpicWeapon != null)
            Destroy(generatedEpicWeapon);
        int randomNum = Random.Range(1, 6);
        if (randomNum < 5)
            return;

        int rand = Random.Range(0, EpicGuns.Count);
        if (rand == 0)
        {
            Transform grenadeTransform = rmg.GetRandomOpenTile();
            generatedEpicWeapon = Instantiate(EpicGuns[0].gameObject, grenadeTransform.position, Quaternion.identity);
        }
        else if(rand == 1)
        {
            Transform turretLauncher = rmg.GetRandomOpenTile();
            generatedEpicWeapon = Instantiate(EpicGuns[1].gameObject, turretLauncher.position, Quaternion.identity);
        }
    }

    public void RecieveEpicWeapon(GameObject weapon)
    {
        if (weapon.tag == "grenade launcher")
        {
            EpicGun gun = weapon.GetComponent<EpicGun>();
            EquipEpicWeapon(weapon);
            generatedEpicWeapon = null;
        }
        else if(weapon.tag == "turret")
        {
            tur = Instantiate(turretSetter, transform.position, weaponHoldPoint.rotation, transform);
            StartCoroutine("setTurret");
        }
    }
    public void SetTurret()
    {
        if (canSetTurret)
        {
            canSetTurret = false;
            tur.GetComponent<turretSetter>().SetTurret();
            ts = null;
        }
    }
    IEnumerator setTurret()
    {
        yield return new WaitForSeconds(0.2f);
        canSetTurret = true;
    }
}
