﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Sound 
{
    public string name;
    public AudioClip clip;

    [Range(0f, 1)]
    public float volume;

    [Range(0.1f, 3)]
    public float pitch;

    [HideInInspector]
    public AudioSource source;

    public bool loop;

    //1-music, 2-shoot, 3-reload
    //public int musicType;
}

