﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour
{
    public float DelayTime = 3;
    public Transform anotherTeleport;
    Teleport tel2;

    public bool canUse = true;

    Color originalColor;
    Material mat;
    public Color ReloadingTeleportColor;
    public ParticleSystem effect;

    bool canUseTeleport;

    Transform player;

    [HideInInspector] public RandomGameUI canvas;
    void Start()
    {
        tel2 = anotherTeleport.gameObject.GetComponent<Teleport>();
        mat = GetComponent<Renderer>().material;
        originalColor = mat.color;
    }
    void Update()
    {
        if (canUseTeleport && Input.GetKeyDown(KeyCode.E))
            TeleportToAnotherPoint(player);
    }
    void OnTriggerEnter(Collider other)
    {
        if (canUse && other.GetComponent<PlayerController>() != null)
        {
            canvas.ShowTip(true);
            canUseTeleport = true;
            player = other.transform;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<PlayerController>() != null)
        {
            canvas.ShowTip(false);
            canUseTeleport = false;
            player = null;
        }
    }

    void TeleportToAnotherPoint(Transform player)
    {
        player.position = anotherTeleport.position + Vector3.up * 1;
        player.GetComponent<RandomPlayer>().TeleportEffect();
        StartCoroutine("TimeToNextUse");
        tel2.StartCoroutine("TimeToNextUse");
    }

    public IEnumerator TimeToNextUse()
    {
        mat.color = ReloadingTeleportColor;
        canUse = false;
        effect.Stop();
        StartCoroutine("Reloading");
        canUseTeleport = false;
        yield return new WaitForSeconds(DelayTime);
        canUse = true;
        effect.Play();
    }

    IEnumerator Reloading()
    {
        float percent = 0;
        float speed = 1 / DelayTime;
        Color initialColor = mat.color;
        while (percent < 1)
        {
            percent += Time.deltaTime * speed;
            mat.color = Color.Lerp(initialColor, originalColor, percent);
            yield return null;
        }
    }


}
