﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Audio;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class MenuUI : MonoBehaviour
{
    public Animator anim;

    bool openSceneIsOpen = false;

    Resolution[] resolutions;

    public TMP_Dropdown resolutionDropdown;
    public TMP_Dropdown qualityDropdown;

    bool openSettings = false;

    CanvasScaler cs;


    public AudioMixer audioMixer;
    private void Start()
    {
        Cursor.visible = true;
        Time.timeScale = 1;
        cs = GetComponent<CanvasScaler>();
        
        resolutions = Screen.resolutions;

        resolutionDropdown.ClearOptions();

        List<string> options = new List<string>();
        int curResIndex = 0;
        for (int i = 0; i < resolutions.Length; i++)
        {
            string option = resolutions[i].width + "x" + resolutions[i].height;
            options.Add(option);

            if(resolutions[i].width == Screen.currentResolution.width && 
               resolutions[i].height == Screen.currentResolution.height)
            {
                curResIndex = i;
            }
        }
        resolutionDropdown.AddOptions(options);

        resolutionDropdown.value = curResIndex;
        resolutionDropdown.RefreshShownValue();
        Screen.SetResolution((int)cs.referenceResolution.x, (int)cs.referenceResolution.y, Screen.fullScreen);
        QualitySettings.SetQualityLevel(5);
        qualityDropdown.value = 5;
    }

    public void SelectScene()
    {
        //openSceneIsOpen = !openSceneIsOpen;
        //anim.SetBool("openSelectScene", openSceneIsOpen);
        SceneManager.LoadScene("RandomGame");

    }
    public void OpenMainScene()
    {
        //SceneManager.LoadScene("Test");
    }
    public void OpenRandomScene()
    {
        SceneManager.LoadScene("RandomGame");
    }

    public void SetQuality(int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
    }

    public void SetFullscreen(bool isFullscreen)
    {
        Screen.fullScreen = isFullscreen;
    }

    public void SetRes(int resIndex)
    {
        Resolution res = resolutions[resIndex];
        Screen.SetResolution(res.width, res.height, Screen.fullScreen);
        //cs.referenceResolution = new Vector2(res.width, res.height);
    }

    public void OpenSettings()
    {
        openSettings = !openSettings;
        anim.SetBool("Options", openSettings);
    }

    public void Exit()
    {
        Application.Quit();
    }

    public void Volume(float volume)
    {
        audioMixer.SetFloat("volume", volume);
    }
}
