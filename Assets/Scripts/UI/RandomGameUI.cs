﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RandomGameUI : MonoBehaviour
{
    public Image fadePlane;

    public GameObject gameOverUI;

    public RectTransform newWaveBanner;
    public Text newWaveNum;
    public Text newWaveEnemyCount;

    RandomSpawner sp;

    public GameObject pauseMenu;
    public TMP_Text waveNum;
    public TMP_Text EnemiesRemaining;
    bool menuIsOpen = false;

    int currentWaveNumber;

    RandomPlayer player;

    public GameObject pressEToUse;
    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<RandomPlayer>();
        FindObjectOfType<RandomPlayer>().OnDeath += OnGameOver;
        pressEToUse.SetActive(false);
    }
    private void Awake()
    {
        sp = FindObjectOfType<RandomSpawner>();
        sp.OnNewWave += OnNewWave;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Resume();
        }
    }
    void OnNewWave(int waveNum)
    {
        //string[] numbers = { "One", "Two", "Three", "Four", "Five", "Six",};
        newWaveNum.text = "-Wave " + waveNum + "-";
        string enemyCountString = ((sp.waves[waveNum - 1].infinite) ? "Infinite" : sp.waves[waveNum - 1].enemyCount + "");
        newWaveEnemyCount.text = "Enemies:" + sp.waves[waveNum - 1].enemyCount;
        currentWaveNumber = waveNum;
        StopCoroutine("AnimateNewWave");
        StartCoroutine("AnimateNewWave");
    }
    IEnumerator AnimateNewWave()
    {
        float delayTime = 1.5f;
        float speed = 3;
        float animationPercent = 0;
        int dir = 1;

        float endDelayTime = Time.time + 1 / speed + delayTime;
        while (animationPercent >= 0)
        {
            animationPercent += Time.deltaTime * speed * dir;
            if (animationPercent >= 1)
            {
                animationPercent = 1;
                if (Time.time > endDelayTime)
                {
                    dir = -1;
                }
            }

            newWaveBanner.anchoredPosition = Vector2.up * Mathf.Lerp(-400, 0, animationPercent);
            yield return null;
        }
    }
    void OnGameOver()
    {
        Cursor.visible = true;
        StartCoroutine(Fade(Color.clear, Color.black, 1));
        gameOverUI.SetActive(true);
    }

    IEnumerator Fade(Color from, Color to, float time)
    {
        float speed = 1 / time;
        float percent = 0;
        while (percent < 1)
        {
            percent += Time.deltaTime * speed;
            fadePlane.color = Color.Lerp(from, to, percent);
            yield return null;
        }
    }
    #region inputUI
    public void StartNewGame()
    {
        SceneManager.LoadScene("RandomGame");
    }

    public void Resume()
    {
        if (menuIsOpen)
        {
            player.pause = false;
            Time.timeScale = 1;
            pauseMenu.SetActive(false);
            menuIsOpen = false;
        }
        else
        {
            player.pause = true;
            Time.timeScale = 0;
            pauseMenu.SetActive(true);
            menuIsOpen = true;
            Cursor.visible = true;
            waveNum.text = ("-Wave " + currentWaveNumber + "-");
            EnemiesRemaining.text = ("Enemies remaining " + sp.enemiesRemainingAlive.ToString());
            pressEToUse.SetActive(false);
        }

    }
    public void Restart()
    {
        SceneManager.LoadScene("RandomGame");
    }
    public void OpenMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
    public void Quit()
    {
        Application.Quit();
    }
    #endregion

    public void ShowTip(bool setActive)
    {
        pressEToUse.gameObject.SetActive(setActive);
    }
}
