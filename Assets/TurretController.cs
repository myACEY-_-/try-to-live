﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TurretController : MonoBehaviour
{
    public Transform turret;
    public Transform shootPoint;
    public Bullet bulletPrefab;
    Transform selectedEnemy;
    bool hasEnemy = false;

    public float timeBetweenShots;
    float shootTimer;

    [Header("BulletSettings")]
    public float bulletDamage;
    public float bulletSpeed;

    SphereCollider collider;
    float radius;
    public Image image;

    private void Start()
    {
        shootTimer = timeBetweenShots;
        collider = GetComponent<SphereCollider>();
        collider.enabled = true;
        radius = collider.radius;
        image.rectTransform.sizeDelta = new Vector2(radius * 2, radius * 2);
    }

    void CheckAndSelectEnemy(Transform enemy)
    {
        print("+");
        if (selectedEnemy == null) 
        {
            selectedEnemy = enemy;
            hasEnemy = true;
        }
        else
        {
            if (enemy == selectedEnemy)
                return;

            //CheckDistance
            if (Vector3.Distance(enemy.position, transform.position) < Vector3.Distance(selectedEnemy.position, transform.position))
            {
                selectedEnemy = enemy;
                hasEnemy = true;
            }

        }
    }
    private void FixedUpdate()
    {
        shootTimer += Time.deltaTime;
        if (hasEnemy)
        {
            var lookPos = selectedEnemy.position - transform.position;
            lookPos.y = 0;
            var rotation = Quaternion.LookRotation(lookPos);
            turret.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 120);
            if (shootTimer >= timeBetweenShots)
                Shoot();
        }
    }
    void Shoot()
    {
        shootTimer = 0;
        Bullet bullet = Instantiate(bulletPrefab, shootPoint.position, shootPoint.transform.rotation);
        bullet.SetParametrs(bulletSpeed, bulletDamage);

    }
    void OnTriggerStay(Collider other)
    {
        Enemy enemy = other.GetComponent<Enemy>();
        if (enemy != null)
            CheckAndSelectEnemy(enemy.transform);
    }
    private void OnTriggerExit(Collider other)
    {
        Enemy enemy = other.GetComponent<Enemy>();
        if (enemy != null)
        {
            selectedEnemy = null;
            hasEnemy = false;
            if (selectedEnemy = enemy.transform)
            {
                selectedEnemy = null;
                hasEnemy = false;
            }
        }

    }

    public void EnemyKilled(Transform enemy)
    {
        if(enemy = selectedEnemy)
        {
            selectedEnemy = null;
            hasEnemy = false;
        }
    }
}
