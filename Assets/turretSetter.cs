﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class turretSetter : MonoBehaviour
{
    public GameObject turret;
    public Transform point;

    public void SetTurret() 
    {
        GameObject tur = Instantiate(turret, point.position, Quaternion.identity);
        Destroy(gameObject);
    }
}
